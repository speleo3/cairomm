image: ubuntu:devel

stages:
  - build
  - deploy

variables:
  MESON_GCC_DEPS: g++
                  gettext
                  git
                  yelp-tools
                  python3-pygments
                  python3-setuptools
                  libcairo2-dev
                  mm-common
                  meson
                  ninja-build
  GIT_SUBMODULE_STRATEGY: normal

# In glibmm a cache is used for copying libsigc/ from a build_sigc job to
# jobs that build glibmm. That's not possible in cairomm.
# It looks like a cache can be used only locally within a job.
# Result in the build_sigc job:
#   No URL provided, cache will be not uploaded to shared cache server.
#   Cache will be stored only locally.
# Result in subsequent jobs that try to download the cached files:
#   ninja: fatal: chdir to 'libsigc/_build' - No such file or directory

.build_default:
  before_script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt update && apt -y upgrade && apt -y install $DEPENDENCIES
    - git clone --branch 3.0.7 --depth 1 https://github.com/libsigcplusplus/libsigcplusplus libsigc
    - cd libsigc
    - mkdir _build && cd _build
    - meson --prefix=/usr --libdir=lib -Dwarnings=fatal -Dbuild-examples=false -Dbuildtype=release
    - meson compile
    - meson install
    - cd ../..

release_gcc_build:
  extends: .build_default
  stage: build
  variables:
    DEPENDENCIES: $MESON_GCC_DEPS
  script:
    - mkdir _build && cd _build
    # -Ddebug=false + -Doptimization=3 correspond to -Dbuildtype=release
    - meson --prefix=/usr --libdir=lib -Ddebug=false -Doptimization=3 -Dwarnings=fatal
    - meson compile
    - meson test
    - meson install
  artifacts:
    when: always
    paths:
      - _build/docs/reference

release_clang_build:
  extends: .build_default
  stage: build
  variables:
    DEPENDENCIES: $MESON_GCC_DEPS clang
  script:
    - mkdir _build && cd _build
    # -Ddebug=false + -Doptimization=3 correspond to -Dbuildtype=release
    - CC=clang CXX=clang++ meson --prefix=/usr --libdir=lib -Ddebug=false -Doptimization=3 -Dwarnings=fatal
    - meson compile
    - meson test
    - meson install
  allow_failure: true
  artifacts:
    when: on_failure
    paths:
      - _build/meson-logs/testlog.txt
      - _build/meson-logs/meson-log.txt
    expire_in: 1 week

# Publish reference documentation at cairo.pages.freedesktop.org/cairomm
pages:
  stage: deploy
  needs: [release_gcc_build]
  script:
    - mkdir public
    - mv _build/docs/reference/html/* public
  artifacts:
    paths:
      - public
  only:
    refs:
      - master
